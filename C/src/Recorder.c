/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "hdf5.h"
#include "hdf5_hl.h"

#include "ModelicaUtilities.h"

#include "HDF5Recorder.h"

// dataset names for recording
#define TIME				"time"
#define SIGNAL_NAMES		"signal_names"
#define SIGNAL_QUANTITIES	"signal_quantities"
#define SIGNAL_UNITS		"signal_units"
#define SIGNAL_VALUES		"signal_values"
#define SIGNAL_COMMENTS		"signal_comments"

#define COMMENT_ATTR_NAME	"COMMENT"
#define UNIT_ATTR_NAME		"UNIT"
#define QUANTITY_ATTR_NAME	"QUANTITY"

#define ASSERT_NO_ERROR(_ret)	if((_ret) < 0)	{ status = -1; goto out; }
#define ASSERT_VALID_ID(_id)	if((_id) < 0)	{ status = -1; goto out; }
#define ASSERT_TRUE(_cond)		if(!(_cond))	{ status = -1; goto out; }

static void free_string_array(size_t len, char **arr) {
	size_t i;
	for(i = 0; i < len; i++) free(arr[i]);
	free(arr);
}

struct ModelicaHDF5Recorder_recorder_t {
	// name of the result and SDF files
	char *		filename;
	char *		sdf_filename;

	// general
	hid_t		file;
	
	// time
	hid_t		time_dataspace;
	hid_t		time_dataset;
	hsize_t		time_dims[1];		
    hsize_t     time_size[1];
	hsize_t     time_offset[1];

	// signals
	hid_t		real_dataspace;
	hid_t		real_dataset;
	hsize_t		real_dims[2];		
    hsize_t     real_size[2];
    hsize_t     real_offset[2];
};


static int save_strings(hid_t file, hsize_t dims[], hid_t filetype, hid_t memtype, const char *dset_name, const char **strings) {
	hid_t dset, space;
	herr_t status;
	
	// Create dataspace.  Setting maximum size to NULL sets the maximum size to be the current size.
    space = H5Screate_simple (1, dims, NULL);

    // Create the dataset and write the variable-length string data to
    dset = H5Dcreate (file, dset_name, filetype, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	status = H5Dwrite (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, strings);
	
	status = H5Sclose (space);
	status = H5Dclose (dset);

    return status;
}

ModelicaHDF5Recorder_recorder_h HDF5Recorder_open(
	const char* filename, 
	int num_signals, 
	const char **signal_names, 
	const char **signal_quantities, 
	const char **signal_units, 
	const char **signal_comments, 
	const char *sdf_filename) {

	hid_t	 filetype = H5I_INVALID_HID;
	hid_t	 memtype  = H5I_INVALID_HID;
	hid_t	 prop	  = H5I_INVALID_HID;
	hsize_t  names_dims[1];
	hsize_t  time_maxdims[1];
	hsize_t  real_maxdims[2];
	herr_t   status = 0;

	ModelicaHDF5Recorder_recorder_h rec;
	
	rec = (ModelicaHDF5Recorder_recorder_h) malloc(sizeof(*rec));

	if(rec == NULL)
		return NULL;

	// TODO: check arguments
	rec->filename = strdup(filename);

	// store the SDF filename
	if(sdf_filename != NULL && strlen(sdf_filename) > 0) {
		rec->sdf_filename = strdup(sdf_filename);
	} else
		rec->sdf_filename = NULL;

	// names
	names_dims[0] = num_signals;

	// time
	time_maxdims[0] = H5S_UNLIMITED;
	rec->time_dims[0] = 1;
	rec->time_offset[0] = 0;

	// signals
	rec->real_dims[0] = 1;
	rec->real_dims[1] = num_signals;
	real_maxdims[0] = H5S_UNLIMITED;
	real_maxdims[1] = num_signals;	
	rec->real_offset[0] = 0;
	rec->real_offset[1] = 0;

    // create a new file (if the file exists its contents will be overwritten)
    if((rec->file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)) < 0) { status = -1; goto out; }

	// Create file and memory datatypes
	if((filetype = H5Tcopy (H5T_FORTRAN_S1)) < 0) { status = -1; goto out; }
    if(H5Tset_size (filetype, H5T_VARIABLE) != 0) { status = -1; goto out; }
    if((memtype = H5Tcopy (H5T_C_S1)) < 0)		  { status = -1; goto out; }
    if(H5Tset_size (memtype, H5T_VARIABLE) != 0)  { status = -1; goto out; }

	if(save_strings(rec->file, names_dims, filetype, memtype, SIGNAL_NAMES, signal_names) != 0)		   { status = -1; goto out; }
	if(save_strings(rec->file, names_dims, filetype, memtype, SIGNAL_QUANTITIES, signal_quantities) != 0) { status = -1; goto out; }
	if(save_strings(rec->file, names_dims, filetype, memtype, SIGNAL_UNITS, signal_units) != 0)		   { status = -1; goto out; }
	if(save_strings(rec->file, names_dims, filetype, memtype, SIGNAL_COMMENTS, signal_comments) != 0)	   { status = -1; goto out; }

	if(H5Tclose (filetype) != 0) { status = -1; goto out; }
    if(H5Tclose (memtype) != 0)  { status = -1; goto out; }

	// time
	if((rec->time_dataspace = H5Screate_simple (1, rec->time_dims, time_maxdims)) < 0) { status = -1; goto out; }
	if((prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) { status = -1; goto out; }
	if(H5Pset_chunk (prop, 1, rec->time_dims) != 0)  { status = -1; goto out; }
	if((rec->time_dataset = H5Dcreate2 (rec->file, TIME, H5T_NATIVE_DOUBLE, rec->time_dataspace, H5P_DEFAULT, prop, H5P_DEFAULT)) < 0) { status = -1; goto out; } 
	if(H5Pclose (prop) != 0) { status = -1; goto out; }

	// signals
    if((rec->real_dataspace = H5Screate_simple (2, rec->real_dims, real_maxdims)) < 0) { status = -1; goto out; }
	if((prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) { status = -1; goto out; }
    if(H5Pset_chunk (prop, 2, rec->real_dims) != 0)  { status = -1; goto out; }
	if((rec->real_dataset = H5Dcreate2 (rec->file, SIGNAL_VALUES, H5T_NATIVE_DOUBLE, rec->real_dataspace, H5P_DEFAULT, prop, H5P_DEFAULT)) < 0) { status = -1; goto out; }
	if(H5Pclose (prop) != 0) { status = -1; goto out; }

out:
	if(status == 0) {
		return (ModelicaHDF5Recorder_recorder_h)rec;
	} else {
		free(rec);
		return NULL;
	}
}

int HDF5Recorder_record(ModelicaHDF5Recorder_recorder_h recorder, double time, int num_signals, const double *signal_values) {
	herr_t	status = 0;
	hid_t	memspace  = H5I_INVALID_HID;
	hid_t	dataspace = H5I_INVALID_HID;
	ModelicaHDF5Recorder_recorder_h rec = recorder;

	rec->time_size[0] = rec->time_offset[0] + rec->time_dims[0];

	if(H5Dset_extent (rec->time_dataset, rec->time_size) != 0) { status = -1; goto out; }
	if((dataspace = H5Dget_space (rec->time_dataset)) < 0) { status = -1; goto out; }
	if(H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, rec->time_offset, NULL, rec->time_dims, NULL) != 0) { status = -1; goto out; } 
	if((memspace = H5Screate_simple (1, rec->time_dims, NULL)) < 0) { status = -1; goto out; }
	if(H5Dwrite (rec->time_dataset, H5T_NATIVE_DOUBLE, memspace, dataspace, H5P_DEFAULT, &time) != 0) { status = -1; goto out; }
	if(H5Sclose (memspace) != 0) { status = -1; goto out; }
	if(H5Sclose(dataspace) != 0) { status = -1; goto out; }
	rec->time_offset[0]++;

	// extend the dataset
	rec->real_size[0] = rec->real_offset[0] + rec->real_dims[0];
	rec->real_size[1] = rec->real_dims[1];

	if(H5Dset_extent (rec->real_dataset, rec->real_size) != 0) { status = -1; goto out; }
	
	// select a hyperslab in extended portion of dataset
	if((dataspace = H5Dget_space (rec->real_dataset)) < 0) { status = -1; goto out; }
	if(H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, rec->real_offset, NULL, rec->real_dims, NULL) != 0) { status = -1; goto out; }
	if((memspace = H5Screate_simple (2, rec->real_dims, NULL)) < 0) { status = -1; goto out; }
	if(H5Dwrite (rec->real_dataset, H5T_NATIVE_DOUBLE, memspace, dataspace, H5P_DEFAULT, signal_values) != 0) { status = -1; goto out; }
	if(H5Sclose (memspace) != 0) { status = -1; goto out; }
	if(H5Sclose(dataspace) != 0) { status = -1; goto out; }
	rec->real_offset[0]++;

out:
	return status;
}

int HDF5Recorder_close(ModelicaHDF5Recorder_recorder_h recorder) {
	herr_t status = 0;
	ModelicaHDF5Recorder_recorder_h rec = recorder;
	
	if(H5Dclose (rec->time_dataset) != 0)	{ status = -1; }
    if(H5Sclose (rec->time_dataspace) != 0)	{ status = -1; }
	if(H5Dclose (rec->real_dataset) != 0)	{ status = -1; }
    if(H5Sclose (rec->real_dataspace) != 0)	{ status = -1; }
    if(H5Fclose (rec->file) != 0)			{ status = -1; }

	if(rec->sdf_filename != NULL)
		status = ModelicaHDF5Recorder_result2sdf(rec->filename, rec->sdf_filename);

	free(recorder);

	return status;
}

static char ** read_strings(hid_t file, hsize_t *nvalues, const char* dataset_name) {
	hid_t       filetype	= H5I_INVALID_HID;
	hid_t       memtype		= H5I_INVALID_HID;
	hid_t       space		= H5I_INVALID_HID;
	hid_t       dset		= H5I_INVALID_HID;                                          
    herr_t      status		= 0;
    char **		rdata		= NULL; // return buffer
	char **		rbuf		= NULL; // temporary read buffer
    int         ndims, i;

	ASSERT_VALID_ID(dset = H5Dopen (file, dataset_name, H5P_DEFAULT))

    // get the datatype
    ASSERT_VALID_ID(filetype = H5Dget_type (dset))

    // get dataspace and allocate memory for read buffer
	ASSERT_VALID_ID(space = H5Dget_space (dset))
    ndims = H5Sget_simple_extent_dims (space, nvalues, NULL);
	rbuf = (char **) malloc ((*nvalues) * sizeof (char *));
	rdata = (char **) malloc ((*nvalues) * sizeof (char *));

    // create the memory datatype
    ASSERT_VALID_ID(memtype = H5Tcopy (H5T_C_S1))
    ASSERT_NO_ERROR(H5Tset_size (memtype, H5T_VARIABLE))

    // read the data
    ASSERT_NO_ERROR(H5Dread (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf))

	// copy the strings
	for(i = 0; i < *nvalues; i++) {
		rdata[i] = strdup(rbuf[i]);
	}

out:
    /*
     * Close and release resources.  Note that H5Dvlen_reclaim works
     * for variable-length strings as well as variable-length arrays.
     * Also note that we must still free the array of pointers stored
     * in rdata, as H5Tvlen_reclaim only frees the data these point to.
     */
    H5Dvlen_reclaim (memtype, space, H5P_DEFAULT, rbuf);
    H5Dclose (dset);
    H5Sclose (space);
    H5Tclose (filetype);
    H5Tclose (memtype);

    return (status == 0) ? rdata : NULL;
}

// TODO: make this function static
int ModelicaHDF5Recorder_result2sdf(const char* in_file, const char* out_file) {
	hid_t       fid_result	= H5I_INVALID_HID;
	hid_t       filetype	= H5I_INVALID_HID;
	hid_t       memtype		= H5I_INVALID_HID;
	hid_t       space		= H5I_INVALID_HID;
	hid_t       dset		= H5I_INVALID_HID;
	hid_t       fid_sdf		= H5I_INVALID_HID;
	hid_t       ds_time		= H5I_INVALID_HID;
	hid_t       ds_signal	= H5I_INVALID_HID;
	hid_t       memspace	= H5I_INVALID_HID;

    herr_t      status		= 0;
    hsize_t     dims[2]		= { 0, 0 };
	hsize_t     start[2]	= { 0, 0 };
	hsize_t     count[2]	= { 0, 1 };
	H5T_class_t class_id			= H5T_NO_CLASS;
	size_t		type_size			= -1;
    char **		signal_names		= NULL;
	char **		signal_quantities	= NULL;
	char **		signal_units		= NULL;
	char **		signal_comments		= NULL;
    int         i, j;
	int			rank;
	hsize_t		nvalues;
	hsize_t		nsignals;
	hsize_t		nsamples; 
	double *	time_data			= NULL;
	double *	signals_data		= NULL;
	double *	buffer				= NULL;

    // open the result file
	ASSERT_VALID_ID(fid_result = H5Fopen (in_file, H5F_ACC_RDONLY, H5P_DEFAULT))

	// open the time dataset
	ASSERT_NO_ERROR(H5LTget_dataset_ndims (fid_result, TIME, &rank))

	ASSERT_TRUE(rank == 1)

	// TODO: check rank
	ASSERT_NO_ERROR(H5LTget_dataset_info (fid_result, TIME, &nsamples, &class_id, &type_size))
	
	// TODO: check class & size
	ASSERT_TRUE(nsamples > 0)
	ASSERT_TRUE(class_id == H5T_FLOAT)
	ASSERT_TRUE(type_size == 8)

	time_data = (double *) malloc(nsamples * sizeof(double)); 

	ASSERT_NO_ERROR(H5LTread_dataset_double(fid_result, TIME, time_data))

	// open the signals dataset
	ASSERT_NO_ERROR(H5LTget_dataset_ndims (fid_result, SIGNAL_VALUES, &rank))
	
	// check rank
	ASSERT_TRUE(rank == 2)

	status = H5LTget_dataset_info (fid_result, SIGNAL_VALUES, dims, &class_id, &type_size);
	// check size and type
	ASSERT_TRUE(dims[0] == nsamples)
	ASSERT_TRUE(dims[1] >= 1)
	ASSERT_TRUE(class_id == H5T_FLOAT)
	ASSERT_TRUE(type_size == 8)

	nsignals = dims[1];

	signal_names = read_strings(fid_result, &nvalues, SIGNAL_NAMES);
	ASSERT_TRUE(nvalues == nsignals)

	signal_quantities = read_strings(fid_result, &nvalues, SIGNAL_QUANTITIES);
	ASSERT_TRUE(nvalues == nsignals)

	signal_units = read_strings(fid_result, &nvalues, SIGNAL_UNITS);
	ASSERT_TRUE(nvalues == nsignals)

	signal_comments	= read_strings(fid_result, &nvalues, SIGNAL_COMMENTS);
	ASSERT_TRUE(nvalues == nsignals)

	signals_data = (double *) malloc(nsamples * nsignals * type_size);
	buffer = (double *) malloc(nsamples * type_size);

	dset = H5Dopen (fid_result, SIGNAL_VALUES, H5P_DEFAULT);

	// write the output file
	fid_sdf = H5Fcreate(out_file, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	ASSERT_NO_ERROR(H5LTmake_dataset_double(fid_sdf, TIME, 1, &nsamples, time_data))

	ASSERT_VALID_ID(ds_time = H5Dopen(fid_sdf, TIME, H5P_DEFAULT))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", QUANTITY_ATTR_NAME, "Time"))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", UNIT_ATTR_NAME, "s"))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", COMMENT_ATTR_NAME, "Time"))

	ASSERT_VALID_ID(space = H5Dget_space (dset))

	// create a dataspace to read the signals
	ASSERT_VALID_ID(memspace = H5Screate_simple(1, &nsamples, NULL))

	for(i = 0; i < nsignals; i++) {
		// select the hyperslab to use for reading
		start[1] = i;
		count[0] = nsamples;
		ASSERT_NO_ERROR(H5Sselect_hyperslab (space, H5S_SELECT_SET, start, NULL, count, NULL))

		// read the data using the previously defined hyperslab
		ASSERT_NO_ERROR(H5Dread (dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, buffer))

		// write the dataset
		ASSERT_NO_ERROR(H5LTmake_dataset_double(fid_sdf, signal_names[i], 1, &nsamples, buffer))

		// set the quantity, unit and comment attributes
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], QUANTITY_ATTR_NAME, signal_quantities[i]))
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], UNIT_ATTR_NAME, signal_units[i]))
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], COMMENT_ATTR_NAME, signal_comments[i]))

		// attach the time as scale
		ASSERT_VALID_ID(ds_signal = H5Dopen(fid_sdf, signal_names[i], H5P_DEFAULT))
		ASSERT_NO_ERROR(H5DSattach_scale(ds_signal, ds_time, 0))
		ASSERT_NO_ERROR(H5Dclose(ds_signal))
	}

out:
	// sdf file
	status = H5Sclose(memspace);
	status = H5Dclose(ds_time);
	status = H5Fclose(fid_sdf);

	// result file
	status = H5Dclose(dset);
	status = H5Fclose(fid_result);

	free(time_data);

	free_string_array(nvalues, signal_names);
	free_string_array(nvalues, signal_quantities);
	free_string_array(nvalues, signal_units);
	free_string_array(nvalues, signal_comments);

    return status;
}

ModelicaHDF5Recorder_recorder_h ModelicaHDF5Recorder_open(const char* filename, int num_signals, const char **signal_names, const char **signal_quantities, const char **signal_units, const char **signal_comments, const char *sdf_filename) {
	ModelicaHDF5Recorder_recorder_h recorder = HDF5Recorder_open(filename, num_signals, signal_names, signal_quantities, signal_units, signal_comments, sdf_filename);
	
#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaHDF5Recorder_open('%s', %d, ..., '%s')\n", filename, num_signals, sdf_filename);
#endif
	
	if(recorder == NULL)
		ModelicaError("Failed to open recorder");

	return recorder;
}

void ModelicaHDF5Recorder_record(ModelicaHDF5Recorder_recorder_h recorder, double time, int num_signals, const double *signal_values) {

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaHDF5Recorder_record(%f, %d, ...)\n", time, num_signals);
#endif

	if(HDF5Recorder_record(recorder, time, num_signals, signal_values) < 0)
		ModelicaFormatMessage("Failed record signals\n");
}

void ModelicaHDF5Recorder_close(ModelicaHDF5Recorder_recorder_h recorder) {

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaHDF5Recorder_close(0x%p)\n", recorder);
#endif

	if(HDF5Recorder_close(recorder) < 0)
		ModelicaError("Failed to close recorder");
}