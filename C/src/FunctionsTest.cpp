/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include "gtest/gtest.h"
#include "ModelicaHDF5Functions.h"

#define GROUPS_FILE "groups.sdf"
#define DATA_FILE "datasets.sdf"
#define DS1 "/ds1"
#define DS2 "/ds2"
#define DS3 "/ds3"

TEST(FunctionsTest, CreateGroup) {
	// create a group with NULL comment
	ModelicaHDF5Functions_create_group(GROUPS_FILE, "/G1", NULL);
	
	// create a group with empty comment
	ModelicaHDF5Functions_create_group(GROUPS_FILE, "/G2", "");

	// create a group with comment
	ModelicaHDF5Functions_create_group(GROUPS_FILE, "/G3", "Group 1");
}

TEST(FunctionsTest, ModelicaHDF5Functions_make_dataset_double) {
	int dims[1] = { 3 };
	double buffer[3] = { 1, 2, 3 };

	ModelicaHDF5Functions_make_dataset_double(NULL, DS1, 1, dims, buffer, "Q1", "U1", "An example dataset");
}

//TEST(FunctionsTest, ModelicaHDF5Functions_attach_scale) {
//	int dims[1] = { 3 };
//	double buffer[3] = { 1, 2, 3 };
//
//	ModelicaHDF5Functions_make_dataset_double(DATA_FILE, DS2, 1, dims, buffer, "Q2", "U2", "Another example dataset");
//	ModelicaHDF5Functions_make_dataset_double(DATA_FILE, DS3, 1, dims, buffer, "Q3", "U3", "An example scale");
//
//	ModelicaHDF5Functions_attach_scale(DATA_FILE, DS2, DS3, "Example dimension", 0);
//}

//TEST(FunctionsTest, ModelicaHDF5Functions_get_dataset_rank) {
//	EXPECT_EQ(1, ModelicaHDF5Functions_get_dataset_rank(DATA_FILE, DS1));
//}

TEST(FunctionsTest, ModelicaHDF5Functions_get_dataset_dims) {
	int dims[1] = { -1 };
	ModelicaHDF5Functions_get_dataset_dims(DATA_FILE, DS1, dims);
	EXPECT_EQ(3, dims[0]);
}

TEST(FunctionsTest, ModelicaHDF5Functions_read_dataset_double) {
	double buffer[3];

	ModelicaHDF5Functions_read_dataset_double(DATA_FILE, DS1, NULL, NULL, buffer);

	EXPECT_DOUBLE_EQ(1, buffer[0]);
	EXPECT_DOUBLE_EQ(2, buffer[1]);
	EXPECT_DOUBLE_EQ(3, buffer[2]);
}

//TEST(FunctionsTest, ModelicaHDF5Functions_read) {
//	int ds_id = -1;
//	char *scaleQuantites[1];
//	char *scaleUnits[1];
//	dataset *ds;
//
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(NULL, "/ds2", 1, NULL, NULL, NULL, NULL, 1));
//	ModelicaHDF5Functions_get_error_message(MAX_MESSAGE_LENGTH, error_message);
//	EXPECT_STREQ("The file name must not be NULL", error_message);
//
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, NULL, 1, NULL, NULL, NULL, NULL, 1));
//	ModelicaHDF5Functions_get_error_message(MAX_MESSAGE_LENGTH, error_message);
//	EXPECT_STREQ("The dataset name must not be NULL", error_message);
//
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", -1, NULL, NULL, NULL, NULL, 1));
//	ModelicaHDF5Functions_get_error_message(MAX_MESSAGE_LENGTH, error_message);
//	EXPECT_STREQ("The number of dimensions must be in the range [0; 32]", error_message);
//
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 33, NULL, NULL, NULL, NULL, 1));
//	ModelicaHDF5Functions_get_error_message(MAX_MESSAGE_LENGTH, error_message);
//	EXPECT_STREQ("The number of dimensions must be in the range [0; 32]", error_message);
//
//	// read the dataset
//	ds_id = ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, NULL, 0);
//	EXPECT_GE(ds_id, 0);
//
//	// check if the quantities and units have been loaded
//	ds = ModelicaHDF5Functions_get_dataset(ds_id);
//	EXPECT_STREQ("Q2", ds->dataQuantity);
//	EXPECT_STREQ("U2", ds->dataUnit);
//
//	// read the same dataset with share = TRUE
//	EXPECT_EQ(ds_id, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, NULL, 1));
//
//	// read the same dataset with share = TRUE and invalid rank
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 2, NULL, NULL, NULL, NULL, 1));
//
//	// read the same dataset with share = TRUE and invalid data quantity
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, "?", NULL, NULL, NULL, 1));
//
//	// read the same dataset with share = FALSE
//	EXPECT_NE(ds_id, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, NULL, 0));
//
//	// read dataset with invalid data quantity
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, "?", NULL, NULL, NULL, 0));
//
//	// read dataset with valid data quantity
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, "Q2", NULL, NULL, NULL, 0), 0);
//
//	// read dataset with invalid data unit
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, "?", NULL, NULL, 0));
//
//	// read dataset with valid data unit
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, "U2", NULL, NULL, 0), 0);
//
//	// read dataset with NULL scale quantity
//	scaleQuantites[0] = NULL;
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, (const char **)scaleQuantites, NULL, 0), 0);
//
//	// read dataset with empty scale quantity
//	scaleQuantites[0] = "";
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, (const char **)scaleQuantites, NULL, 0), 0);
//
//	// read dataset with invalid scale quantity
//	scaleQuantites[0] = "?";
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, (const char **)scaleQuantites, NULL, 0));
//	ModelicaHDF5Functions_get_error_message(MAX_MESSAGE_LENGTH, error_message);
//
//	// read dataset with valid scale quantity
//	scaleQuantites[0] = "Q3";
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, (const char **)scaleQuantites, NULL, 0), 0);
//
//	// read dataset with NULL scale unit
//	scaleUnits[0] = NULL;
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0), 0);
//
//	// read dataset with empty scale unit
//	scaleUnits[0] = "";
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0), 0);
//
//	// read dataset with invalid scale unit
//	scaleUnits[0] = "?";
//	EXPECT_EQ(-1, ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0));
//
//	// read dataset with valid scale unit
//	scaleUnits[0] = "U3";
//	EXPECT_GE(ModelicaHDF5Functions_read(DATA_FILE, "/ds2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0), 0);
//}