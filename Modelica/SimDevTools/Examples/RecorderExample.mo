within SimDevTools.Examples;
model RecorderExample
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.Sine sine(freqHz=1)
    annotation (Placement(transformation(extent={{-40,-50},{-20,-30}})));
  Modelica.Blocks.Sources.Pulse pulse(period=1)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  Modelica.Blocks.Sources.SawTooth sawTooth(period=1)
    annotation (Placement(transformation(extent={{-40,30},{-20,50}})));
  Recorder recorder(
    nin=3,
    signalNames={"DS1","DS2","DS3"},
    signalQuantities={"Q1","Q2","Q3"},
    signalUnits={"U1","U2","U3"},
    signalComments={"Comment 1","Comment 2","Comment 3"})
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
equation
  connect(sine.y, recorder.u[1]) annotation (Line(
      points={{-19,-40},{0,-40},{0,-1.33333},{18,-1.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pulse.y, recorder.u[2]) annotation (Line(
      points={{-19,0},{0,0},{0,1.11022e-016},{18,1.11022e-016}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sawTooth.y, recorder.u[3]) annotation (Line(
      points={{-19,40},{0,40},{0,1.33333},{18,1.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end RecorderExample;
