within SimDevTools.Functions;
function writeIntegerMatrix "Write an Integer[:,:] to an HDF5 file"
  input String fileName "File Name";
  input String datasetName "Dataset Name";
  input Integer values[:,:] "Values";
  input String quantity = "" "Quantity (optional)";
  input String unit = "" "Unit (optional)";
  input String comment = "" "Comment (optional)";
protected
  Integer dims[:] = { size(values, 1), size(values, 2)};
external "C" ModelicaHDF5Functions_make_dataset_int(fileName, datasetName, 2, dims, values, quantity, unit, comment) annotation (
  Include="#include \"ModelicaHDF5Functions.h\"",
  Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
end writeIntegerMatrix;
