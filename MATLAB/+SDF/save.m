% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

function save(filename, root)

if exist(fullfile(cd, filename), 'file') == 2
    delete(filename)
end 

try
    file_id = H5F.open(filename, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
catch e
    file_id = H5F.create (filename, 'H5F_ACC_TRUNC', 'H5P_DEFAULT', 'H5P_DEFAULT');
end

write_attribute(file_id, 'COMMENT', root.comment);

write_group(file_id, root);

H5F.close (file_id);

end

function write_group(file_id, g)

    scales = [];
    scale_ids = [];
    
    % write the attributes
    fields = fieldnames(g.attributes);
    for i = 1:numel(fields)
        write_attribute(file_id, fields{i}, g.attributes.(fields{i}));
    end
    
    % save the scales first
    for i = 1:numel(g.datasets)
        ds = g.datasets(i);
        
        if ds.is_scale
            ds_id = write_dataset(file_id, ds);
            scales = [scales ds];
            scale_ids = [scale_ids ds_id];            
            H5DS.set_scale(ds_id, ds.scale_name);            
        end

    end
    
    % then the datasets
    for i = 1:numel(g.datasets)
        ds = g.datasets(i);
        
        if ~ds.is_scale
            ds_id = write_dataset(file_id, ds);

            % attach the scales
            for j = 1:numel(g.datasets(i).scales)
                for k = 1:numel(scales)
                    if isequal(scales(k), ds.scales(j)) 
                        H5DS.attach_scale(ds_id, scale_ids(k), j-1);
                        break
                    end
                end
            end
            
            H5D.close (ds_id);
        end
        
    end
end

function ds_id = write_dataset(file_id, ds)
    
    dims = size(ds.data);
    
    if numel(dims) == 2 && dims(1) == 1
        dims = dims(2);
    end
    
    % MATLAB automatically removes the last dimension
    % if it has extent one. Re-add the dummy dimension if
    % a scale is attached
    if numel(dims) < numel(ds.scales)
        dims = [dims 1];
    end
    
    space_id = H5S.create_simple (numel(dims), dims, []);
    ds_id = H5D.create (file_id, ds.name, 'H5T_NATIVE_DOUBLE', space_id, 'H5P_DEFAULT');
    
    % convert the elements in matrix A from row to column major format
    data = permute(ds.data, fliplr(1:ndims(ds.data)));
    
    H5D.write (ds_id, 'H5T_NATIVE_DOUBLE', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);
    
    write_attribute(ds_id, 'COMMENT', ds.comment);
    write_attribute(ds_id, 'QUANTITY', ds.quantity);
    write_attribute(ds_id, 'UNIT', ds.unit);
    if strcmp(ds.display_unit, ds.unit) ~= 1
        write_attribute(ds_id, 'DISPLAY_UNIT', ds.display_unit);
    end
    
    H5S.close (space_id);
end

function write_attribute(obj_id, name, value)
    if ischar(value)

        % encode as UTF-8
        value = char(unicode2native(value, 'UTF-8'));
                
        len = numel(value);    

        type_id = H5T.copy('H5T_FORTRAN_S1');
        H5T.set_size(type_id, len);

        space_id = H5S.create('H5S_SCALAR');
        attr_id = H5A.create(obj_id, name, type_id, space_id, 'H5P_DEFAULT');
        H5S.close(space_id);
        
        H5A.write(attr_id, type_id, value);

        H5A.close(attr_id);
    end
end