% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

classdef Group
    
    properties
        name
        comment
        attributes
        datasets
    end
    
%     methods
%         
%         function self = Group(varargin)
%             if nargin > 0, self.name       = varargin{1}; end
%             if nargin > 1, self.comment    = varargin{2}; end
%             if nargin > 2, self.attributes = varargin{3}; else self.attributes = struct(); end
%             if nargin > 3, self.datasets   = varargin{4}; end
%         end
%         
%     end
    
end