% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

classdef Dataset
    
    properties
        name
        comment
        attributes
        data
        display_name
        scale_name
        quantity
        unit
        display_unit
        is_scale
        scales
    end
    
    properties (Dependent)
        rank
        display_data
    end
    
    methods

        function self = Dataset(varargin)
            self.attributes = struct();
            self.is_scale = 0;
        end
        
        function value = get.rank(obj)
            s = size(obj.data);
            nd = ndims(obj.data);
            
            if nd > 2
                value = nd;
            elseif s(1) <= 1 && s(2) <= 1
                value = 0;
            elseif s(1) == 1 || s(2) == 1
                value = 1;
            else
                value = nd;
            end
        end
        
        function obj = set.display_data(obj, value)
            obj.data = value;
        end
        
        function value = get.display_data(obj)
            if isempty(obj.display_unit)
                value = obj.data;
            else
                value = convert_unit(obj.data, obj.unit, obj.display_unit);
            end
        end
        
        function value = get.display_unit(obj)
            if ~isempty(obj.display_unit), value = obj.display_unit; else value = obj.unit; end
        end
        
    end
    
end